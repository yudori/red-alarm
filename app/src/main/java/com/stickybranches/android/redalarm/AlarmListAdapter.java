package com.stickybranches.android.redalarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;
import com.stickybranches.android.redalarm.database.AlarmDbHelper;
import com.stickybranches.android.redalarm.database.AlarmDbManager;

import java.util.List;
import java.util.TreeSet;

/**
 * Created by Richard O on 1/23/2015.
 */
public class AlarmListAdapter extends BaseAdapter{

    AlarmDbHelper dh;
    List<RedAlarm> alarmList;
    Activity activity;
    AlarmManager alarmManager;

    public AlarmListAdapter(Activity activity){
        this.activity = activity;
        dh = new AlarmDbHelper(activity);
        alarmList = AlarmDbManager.executeReadAll(dh);
        alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
    }

    public void reInitialize(){
        alarmList.clear();
        alarmList.addAll(AlarmDbManager.executeReadAll(dh));
    }

    @Override
    public int getCount() {
        return alarmList.size();
    }

    @Override
    public Object getItem(int position) {
        return alarmList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return alarmList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.alarm_list_item, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.alarm_message);
        ImageView repeat = (ImageView) convertView.findViewById(R.id.repeat_icon);
        TextView days = (TextView) convertView.findViewById(R.id.days_list);
        TextView time = (TextView) convertView.findViewById(R.id.alarm_time);
        LinearLayout ll = (LinearLayout) convertView.findViewById(R.id.item_container);
        CheckBox enableCheckBox = (CheckBox) convertView.findViewById(R.id.enabledCheckBox);
        RedAlarm alarm = alarmList.get(position);

        //link enable button
        AlarmEnabledListener l = new AlarmEnabledListener(alarm);
        enableCheckBox.setOnCheckedChangeListener(l);
        if(alarm.isEnabled()){
            enableCheckBox.setChecked(true);
        }else{
            enableCheckBox.setChecked(false);
        }
        //set title
        title.setText(alarm.getMessage());

        //set repeat image
        if(alarm.isEnableRepeat())
            repeat.setImageResource(R.drawable.repeat_icon);


        //set chosen days of the week
        String chosenDays = "";
        TreeSet<RedAlarm.Day> alarmDays = alarm.getActiveDaysOfWeek();
        days.setText(alarmDays.toString());

        //set time
        time.setText(String.format("%02d:%02d", alarm.getOffTime().hour, alarm.getOffTime().minute));


        return convertView;
    }

    private class AlarmEnabledListener implements CompoundButton.OnCheckedChangeListener{
        RedAlarm redAlarm;
        public AlarmEnabledListener(RedAlarm redAlarm){
            this.redAlarm = redAlarm;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){

                redAlarm.schedule(alarmManager,dh);
                Toast.makeText(activity.getApplicationContext(),"Enabled", Toast.LENGTH_SHORT);
            }else{
                redAlarm.cancel(alarmManager,dh);
                Toast.makeText(activity.getApplicationContext(),"Disabled", Toast.LENGTH_SHORT);

            }
        }
    }
}
