package com.stickybranches.android.redalarm.wake_up_activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;

import com.stickybranches.android.redalarm.R;
import com.stickybranches.android.redalarm.basic_entities.RandomInterpolator;

/**
 * Created by Gerald on 03/03/2015.
 */
public class DodgyButtonActivity extends WakeUpActivity {
    Button stopButton;
    ObjectAnimator animX;
    ObjectAnimator animY;
    AnimatorSet animSetXY;
    @Override
    protected void onCreateWakeUpTest(Bundle savedInstanceState) {
        setContentView(R.layout.layout_dodgy_button_wakeup_activity);
        TextView alarmMessage = (TextView) findViewById(R.id.alarmMessageTextView);
        setAlarmMessageTextView(alarmMessage);
        stopButton = (Button) findViewById(R.id.stopTestButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopTest();
            }
        });
        final View parent = (View) stopButton.getParent();
        animSetXY  = new AnimatorSet();

        ViewTreeObserver observer = parent.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(animSetXY.isStarted()){
                    animSetXY.cancel();
                }
                int width = parent.getMeasuredWidth();
                int height = parent.getMeasuredHeight();
                animX = ObjectAnimator.ofFloat(stopButton, "x",width);
                animY = ObjectAnimator.ofFloat(stopButton,"y", height);
                animX.setInterpolator(new RandomInterpolator(System.currentTimeMillis()));
                animX.setDuration(6000);
                animX.setRepeatCount(ValueAnimator.INFINITE);
                animY.setDuration(6000);
                animY.setRepeatCount(ValueAnimator.INFINITE);
                animY.setInterpolator(new RandomInterpolator(System.currentTimeMillis()*3%26));


                animX.setFloatValues(0, parent.getWidth());
                animY.setFloatValues(0, parent.getHeight());

                animSetXY.playTogether(animX, animY);
                animSetXY.start();

            }
        });
        


    }

    @Override
    protected void onResume() {
        super.onResume();



    }
}
