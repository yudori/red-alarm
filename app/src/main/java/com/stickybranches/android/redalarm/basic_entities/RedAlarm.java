package com.stickybranches.android.redalarm.basic_entities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.format.Time;
import android.util.Log;

import com.stickybranches.android.redalarm.AlarmReceiver;
import com.stickybranches.android.redalarm.R;
import com.stickybranches.android.redalarm.database.AlarmDbHelper;
import com.stickybranches.android.redalarm.database.AlarmDbManager;

import java.util.Arrays;
import java.util.Calendar;
import java.util.TreeSet;

public class RedAlarm {
    // properties
    private long id;
    private String message;
    private TreeSet<Day> activeDaysOfWeek;
    private Time offTime;
    private int wakeUpTestType;
    private int alarmTone;
    private boolean enableRepeat;
    private boolean enabled;
    private Context context;


    /**
     * Enumeration constant representing days of the week
     */
    public enum Day{
        SUN, MON, TUE, WED, THUR, FRI, SAT
    };

    //constants
    public static final String APP_TONE = "APP TONE";
    public static final String ALARM_MESSAGE = "ALARM MESSAGE";

    public static final String RESOURCE_ROOT_PATH =
            "android.resource://com.stickybranches.android.redalarm/";
    /**
     * Default constructor for Red alarm
     * Initializes all variables to default value.
     * Id is initialized to -1 to show non reference to items in the database
     *
     * @param context activity context
     */
    public RedAlarm(Context context){
        /*
        Supplying fields with default values
         */
        id = -1;
        message = "";
        activeDaysOfWeek = new TreeSet<>();
        offTime = new Time();
        wakeUpTestType = 1;
        alarmTone = 1;
        enableRepeat = false;
        enabled = false;
        this.context = context;

    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getAlarmTone() {
        return alarmTone;
    }

    public void setAlarmTone(int alarmTone) {
        this.alarmTone = alarmTone;
    }

    public int getWakeUpTestType() {
        return wakeUpTestType;
    }

    public void setWakeUpTestType(int wakeUpTestType) {
        this.wakeUpTestType = wakeUpTestType;
    }

    public boolean isEnableRepeat() {
        return enableRepeat;
    }

    public void setEnableRepeat(boolean enableRepeat) {
        this.enableRepeat = enableRepeat;
    }

    /**
     * call this method only when values has to be synced from database.
     * Do not call this method directly.
     * Instead use schedule() to schedule the alarm (and evidently enable it)
     *
     * @return whether the alarm is scheduled or not
     */
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.enabled = isEnabled;
    }

    public Time getOffTime(){ return this.offTime; }

    public long getOffTimeInMillis(){ return this.offTime.toMillis(false); }

    public void setOffTime(Time offTime) { this.offTime = offTime; }

    public void setOffTimeFromMillis(long time ) {this.offTime.set(time);}

    public TreeSet<Day> getActiveDaysOfWeek() {
        return activeDaysOfWeek;
    }

    /**
     * adds a day of the week that the alarm is supposed to be active
     * @param day day of the week to add
     */
    public void addActiveDay(Day day){
        activeDaysOfWeek.add(day);
    }

    /**
     * adds a days of the week that the alarm is supposed to be active
     * @param day array of days to add
     */
    public void addActiveDays(Day[] day){
        activeDaysOfWeek.addAll(Arrays.asList(day));
    }

    /**
     * removes a day of the week that the alarm is supposed to be active
     * @param day day of the week to remove
     */
    public void removeActiveDay(Day day){
        activeDaysOfWeek.remove(day);
    }

    /**
     * removes all active days.
     */
    public void clearActiveDays(){
        activeDaysOfWeek.clear();
    }

    /**
     * Returns the pendingIntent associated with this RedAlarm
     * Alarms are scheduled and canceled with the same PendingIntent.
     * RedAlarm manages this PendingIntent object
     * Uri of the alarm is added to the PendingIntent
     *
     * @return PendingIntent associated with this RedAlarm
     */
    public PendingIntent getPendingIntent(){

        if(id >0) {

            Intent intent = new Intent(context, AlarmReceiver.class);

            intent.setData(getUri()); // TO-DO ADD CONTENT URI FOR ALARM DATA WITH ID here

            if(context.getResources().getBoolean(R.bool.debug)== true){
                Log.d("RED ALARM", String.format("Uri = %s",getUri().toString()));
            }
            return PendingIntent.getBroadcast(context, 0, intent, 0);
        }else{
            return null;
        }

    }

    /**
     * Converts days of the week to integer
     * It represents different days of the week as bits
     * Su-M-Tu-W-Th-F-S
     * 0-0-0-0-0-0-0
     *
     * @return integer value representing the days of the week selected
     */
    public int daysToInteger(){
        /*
        Converts days of the week to integer.
        It represent different days of the weeks as bits
        Su-M-T-W-Th-Fr-S
        0-0-0-0-0-0-0
        Any value selected has a bit value of 1. Deselected values have a bit value of 0
         */
        int value = 0;
        if(activeDaysOfWeek.contains(Day.SUN)){
            value += 64;
        }
        if(activeDaysOfWeek.contains(Day.MON)){
            value += 32;
        }
        if(activeDaysOfWeek.contains(Day.TUE)){
            value += 16;
        }
        if(activeDaysOfWeek.contains(Day.WED)){
            value += 8;
        }
        if(activeDaysOfWeek.contains(Day.THUR)){
            value += 4;
        }
        if(activeDaysOfWeek.contains(Day.FRI)){
            value += 2;
        }
        if(activeDaysOfWeek.contains(Day.SAT)){
            value += 1;
        }

        return value;
    }

    /**
     * Sets active dates given the integer value.
     * It finds out whether bits associated with days of the week are activated or not.
     * It populates days of the week set with activated days
     *
     * @param value integer representing the selected days of the week
     */
    public void setActiveDaysFromInteger(int value){
        /*
        Sets active dates given the integer value.
        Calculates if bit numbers are activated starting from MSB
         */
        clearActiveDays();
        if((value/64) == 1){
            addActiveDay(Day.SUN);
        }
        value = value % 64;
        if((value/32) == 1){
            addActiveDay(Day.MON);
        }
        value = value %32;
        if((value/16) == 1){
            addActiveDay(Day.TUE);
        }
        value = value % 16;
        if((value/8) == 1){
            addActiveDay(Day.WED);
        }
        value = value % 8;
        if((value/4) == 1){
            addActiveDay(Day.THUR);
        }
        value = value % 4;
        if((value/2) == 1){
            addActiveDay(Day.FRI);
        }
        value = value % 2;
        if(value == 1){
            addActiveDay(Day.SAT);
        }


    }

    /**
     * It schedules the alarm with the alarmManager and updates the database.
     * It disables the alarm.
     *
     * @param alarmManager AlarmManager to use for scheduling Alarm
     * @param dbHelper database helper for updating the database
     */
    public void  schedule(AlarmManager alarmManager, AlarmDbHelper dbHelper){

        if(alarmManager!= null) {
            PendingIntent pIntent = getPendingIntent();
            if(pIntent != null) {
                // Set the alarm to start at 8:30 a.m.
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, offTime.hour);
                calendar.set(Calendar.MINUTE, offTime.minute);
                alarmManager.cancel(pIntent);
                enabled = false;
                if(enableRepeat) {
                   alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            AlarmManager.INTERVAL_DAY, pIntent);
                    if(context.getResources().getBoolean(R.bool.debug)== true){
                        Log.d("RED ALARM", "Repeating alarm scheduled");
                    }
                }else{
                    long time = calendar.getTimeInMillis();
                    if(System.currentTimeMillis() > time){
                        time += 1000*60*60*24;
                    }
                    alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pIntent);
                    if(context.getResources().getBoolean(R.bool.debug)== true){
                        Log.d("RED ALARM", "Non-Repeating alarm scheduled");
                    }
                }
                enabled = true;
                AlarmDbManager.executeUpdate(dbHelper,this);
                if(context.getResources().getBoolean(R.bool.debug)== true){
                    Log.d("RED ALARM", "Alarm Updated");
                }
            }
        }

    }

    /**
     * Used for cancelling scheduled alarm.
     * It uses the same PendingIntent for scheduling
     *
     * @param alarmManager AlarmManager to use for cancelling Alarm
     */
    public void cancel(AlarmManager alarmManager, AlarmDbHelper dbHelper){
        PendingIntent pIntent = getPendingIntent();
        if(alarmManager != null){
            if(pIntent != null && enabled) {
                alarmManager.cancel(pIntent);
                enabled = false;
                if(context.getResources().getBoolean(R.bool.debug)== true){
                    Log.d("RED ALARM", "Alarm Canceled");
                }
                AlarmDbManager.executeUpdate(dbHelper,this);
            }
        }
    }

    /**
     * Deletes the RedAlarm object from database
     * cancels alarm if alarm was enabled
     *
     * @param alarmManager AlarmManager used for cancelling schedule
     * @param dbHelper database helper
     */
   public void delete(AlarmManager alarmManager, AlarmDbHelper dbHelper){
       if(id >0) {
           this.cancel(alarmManager, dbHelper);
           AlarmDbManager.executeDelete(dbHelper, id);
       }
   }
    /**
     * Gets the fully qualified class name of the WakeUpTestActivity
     *
     * @return fully qualified class name of the WakeUpTestActivity
     */
    public String getWakeUpActivityClassName(){
        // Gets the class name given its index value (wakeUpTestType

        String [] wakeUpTestClasses = context.getResources().getStringArray(R.array.wakeUpTestActivities);
        if(context.getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", String.format("WakeUp Type = %s", wakeUpTestType));
        }
        if(wakeUpTestType >= 0 && wakeUpTestType < wakeUpTestClasses.length){
            if(context.getResources().getBoolean(R.bool.debug)== true){
                Log.d("RED ALARM", String.format("WakeUp Type = %s", wakeUpTestClasses[wakeUpTestType]));
            }
            return wakeUpTestClasses[wakeUpTestType];
        }
        return null;
    }

    /**
     * Gets the path of the alarm tone
     *
     * @return String representing the path of the alarm tone
     */
    public String getAlarmTonePaths(){
        String [] alarmPaths = context.getResources().getStringArray(R.array.alarmTonePaths);

        if(0 >= alarmTone && alarmTone <= alarmPaths.length){
            if(context.getResources().getBoolean(R.bool.debug)== true){
                Log.d("RED ALARM", String.format("Alarm Path = %s", alarmPaths[alarmTone]));
            }
            return RESOURCE_ROOT_PATH + alarmPaths[alarmTone];
        }
        return null;
    }


    /**
     * Gets the uri representing the alarm on the database
     *
     * @return the uri representing the alarm on the database
     */
    public Uri getUri(){
        Uri.Builder builder = new Uri.Builder();
        builder.authority(AlarmDbHelper.AUTHORITY);
        builder.path(AlarmDbHelper.RED_ALARM_PATH);


        return ContentUris.appendId(builder, id).build();
    }

    /**
     * Changes Day of the Week representation from that specified in Calendar class to that specified in RedAlarm.Day
     *
     * @param calendarDay day of the week in Calendar format
     * @return day of the week in RedAlarm.Day format
     */
    public static Day toRedAlarmDay(int calendarDay){
        switch(calendarDay){
            case Calendar.SUNDAY:{
                return Day.SUN;
            }
            case Calendar.MONDAY:{
                return Day.MON;
            }
            case Calendar.TUESDAY:{
                return Day.TUE;
            }
            case Calendar.WEDNESDAY:{
                return Day.WED;
            }
            case Calendar.THURSDAY:{
                return Day.THUR;
            }
            case Calendar.FRIDAY:{
                return Day.FRI;
            }
            case Calendar.SATURDAY:{
                return Day.SAT;
            }default:{
                return null;
            }

        }
    }



}
