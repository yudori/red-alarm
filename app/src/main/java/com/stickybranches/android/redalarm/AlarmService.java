package com.stickybranches.android.redalarm;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;


public class AlarmService extends Service {


    private MediaPlayer alarmTonePlayer;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String alarmTonePath = intent.getStringExtra(RedAlarm.APP_TONE);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if(getResources().getBoolean(R.bool.debug)== true){
                    Log.d("RED ALARM", String.format("Alarm tone Uri: %s", alarmTonePath));
                }
               alarmTonePlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(alarmTonePath));
                alarmTonePlayer.setLooping(true);
                alarmTonePlayer.setVolume(1,1);
                alarmTonePlayer.start();
            }
        });
        thread.start();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        if(alarmTonePlayer != null){
            alarmTonePlayer.release();
        }
        super.onDestroy();
    }
}
