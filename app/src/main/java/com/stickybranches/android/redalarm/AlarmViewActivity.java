package com.stickybranches.android.redalarm;

import android.app.AlarmManager;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;
import com.stickybranches.android.redalarm.database.AlarmDbHelper;
import com.stickybranches.android.redalarm.database.AlarmDbManager;

import java.util.Calendar;


public class AlarmViewActivity extends ActionBarActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, CompoundButton.OnCheckedChangeListener {

    //components
    private TextView alarmTimeTextView;

    private static final int[] WEEKDAY_BUTTON_IDS_ARRAY = {R.id.sundayButton,R.id.mondayButton,
    R.id.tuesdayButton,R.id.wednesdayButton,R.id.thursdayButton,R.id.fridayButton,
    R.id.saturdayButton};
    private static final int[] WEEKDAY_BUTTON_LABEL_IDS_ARRAY = {R.id.sundayButtonLabel,R.id.mondayButtonLabel,
    R.id.tuesdayButtonLabel, R.id.wednesdayButtonLabel, R.id.thursdayButtonLabel, R.id.fridayButtonLabel,
    R.id.saturdayButtonLabel};
    private static final int[] WEEKDAYS = {R.string.sunday, R.string.monday,R.string.tuesday,R.string.wednesday,
    R.string.thursday,R.string.friday,R.string.saturday};
    private static final int[] WEEKDAYS_UNDERLINED = {R.string.sunday_underlined, R.string.monday_underlined,R.string.tuesday_underlined,R.string.wednesday_underlined,
            R.string.thursday_underlined,R.string.friday_underlined,R.string.saturday_underlined};

    private CheckBox[] weekdayButtons;
    private TextView[] weekdayButtonLabels;
    private Spinner wakeUpActivitySpinner;
    private Spinner alarmToneSpinner;
    private Button previewButton;
    private CheckBox repeatWeeklyButton;
    private EditText alarmMessageEditText;
    private Button saveButton;
    private Button cancelButton;
    private TimePickerDialog timePickerDialog;


    private RedAlarm redAlarm;

    public static final int CREATE = 0;
    public static final int VIEW =1;
    private int mode = CREATE;

    AlarmDbHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_view);
        //initialize objects
        weekdayButtons = new CheckBox[7];
        weekdayButtonLabels = new TextView[7];
        for(int i=0; i<WEEKDAY_BUTTON_IDS_ARRAY.length; ++i){
            weekdayButtons[i] = (CheckBox) findViewById(WEEKDAY_BUTTON_IDS_ARRAY[i]);
            weekdayButtonLabels[i] = (TextView) findViewById(WEEKDAY_BUTTON_LABEL_IDS_ARRAY[i]);
            weekdayButtons[i].setOnCheckedChangeListener(this);
            WeekdayClickListener l = new WeekdayClickListener(i);
            weekdayButtonLabels[i].setOnClickListener(l);
        }
        wakeUpActivitySpinner = (Spinner) findViewById(R.id.wakeUpActivitySpinner);
        ArrayAdapter<String> wakeUpArrayAdapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.wakeUpTestNames));
        wakeUpActivitySpinner.setAdapter(wakeUpArrayAdapter);
        alarmToneSpinner = (Spinner) findViewById(R.id.alarmToneSpinner);

        ArrayAdapter<String> alarmToneArrayAdapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.alarmToneNames));
        alarmToneSpinner.setAdapter(alarmToneArrayAdapter);
        previewButton = (Button) findViewById(R.id.wakeUpActivityPreviewButton);
        previewButton.setOnClickListener(this);
        repeatWeeklyButton = (CheckBox) findViewById(R.id.repeatCheckBox);
        repeatWeeklyButton.setOnCheckedChangeListener(this);
        saveButton = (Button) findViewById(R.id.saveButton);
        cancelButton =(Button)findViewById(R.id.cancelButton);
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        alarmTimeTextView = (TextView) findViewById(R.id.timeTextView);
        alarmTimeTextView.setOnClickListener(this);
        alarmMessageEditText = (EditText) findViewById(R.id.alarmMessageEditText);
        dbHelper = new AlarmDbHelper(this);

        timePickerDialog = new TimePickerDialog(this,this,0,0,true);

        /*
        if intent contains data, then the activity views an existing alarm, otherwise
        it is attempting to create a new one.
         */
        Uri uri = getIntent().getData();
        if(uri == null){
            redAlarm = new RedAlarm(this);
            Time t = new Time();
            t.set(System.currentTimeMillis() + 60 * 60 * 1000);
            redAlarm.setOffTime(t);
            redAlarm.setEnableRepeat(false);
            redAlarm.setMessage("Wake Up Sleepy Head");
            redAlarm.setWakeUpTestType(0);
            redAlarm.setAlarmTone(0);
            mode = CREATE;
        }else{
            long id = ContentUris.parseId(uri);
            redAlarm = AlarmDbManager.executeReadById(dbHelper,id);
            mode = VIEW;
            if(redAlarm == null){
                if(getResources().getBoolean(R.bool.debug)== true){
                    Log.d("RED ALARM", "Uri not contained in database. Exiting...");
                }
                finish();
            }
        }
        syncDataFromRedAlarm();
    }



    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.wakeUpActivityPreviewButton:{
                launchWakeUpActivityPreview();
                break;
            }
            case R.id.saveButton:{
                save();
                break;
            }
            case R.id.cancelButton:{
                cancel();
                break;
            }
            case R.id.timeTextView:{
                showTimePickerDialog();
            }
        }

    }

    private void showTimePickerDialog() {
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Showing time picker dialog");
        }

        timePickerDialog.updateTime(redAlarm.getOffTime().hour,redAlarm.getOffTime().minute);
       timePickerDialog.show();
    }

    private void cancel() {
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Cancelling...");
        }
       finish();
    }

    private void save() {
       switch(mode){
           case CREATE:{
               if(getResources().getBoolean(R.bool.debug)== true){
                   Log.d("RED ALARM", "Saving new alarm");
               }
               syncDataToRedAlarm();
               AlarmDbManager.executeInset(dbHelper, redAlarm);
               redAlarm.schedule((AlarmManager)getSystemService(ALARM_SERVICE),dbHelper);
               Toast.makeText(this,"Alarm Created!!", Toast.LENGTH_SHORT);
               break;
           }
           case VIEW:{
               if(getResources().getBoolean(R.bool.debug)== true){
                   Log.d("RED ALARM", "Updating existing alarm");
               }
               syncDataToRedAlarm();
               AlarmDbManager.executeUpdate(dbHelper, redAlarm);
               redAlarm.schedule((AlarmManager)getSystemService(ALARM_SERVICE),dbHelper);
               Toast.makeText(this,"Alarm Saved!!", Toast.LENGTH_SHORT);
               break;
           }default:;
       }

        finish();
    }



    private void launchWakeUpActivityPreview() {
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Launching Wake Up Test preview");
        }
        String[] wakeUpTestTypes = getResources().getStringArray(R.array.wakeUpTestActivities);
        String wakeUpActivityClassName = wakeUpTestTypes[wakeUpActivitySpinner.getSelectedItemPosition()];
        try {
            Class<?> c = Class.forName(wakeUpActivityClassName);
            if(c!=null){
                Intent intent = new Intent(this, c);
                intent.putExtra(RedAlarm.ALARM_MESSAGE,alarmMessageEditText.getText().toString());
                String [] alarmPaths = getResources().getStringArray(R.array.alarmTonePaths);

               String path =  RedAlarm.RESOURCE_ROOT_PATH + alarmPaths[alarmToneSpinner.getSelectedItemPosition()];

                intent.putExtra(RedAlarm.APP_TONE,path);
                startActivity(intent);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void syncDataFromRedAlarm(){
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Syncing data from red alarm object...");
        }
        for(int i=0; i<7; ++i ){
            weekdayButtons[i].setChecked(false);
            setWeekdayLabelEnabled(i,false);
        }
        if(redAlarm != null){
            alarmTimeTextView.setText(redAlarm.getOffTime().format("%H:%M"));

            RedAlarm.Day[] days = new RedAlarm.Day[1];
            days = redAlarm.getActiveDaysOfWeek().toArray(days);

            if(days!=null) {
                for (RedAlarm.Day day : days) {
                    if(day == null) break;
                    weekdayButtons[dayOfWeekToIndex(day)].setChecked(true);
                    setWeekdayLabelEnabled(dayOfWeekToIndex(day),true);
                }
            }

            wakeUpActivitySpinner.setSelection(redAlarm.getWakeUpTestType());

            alarmToneSpinner.setSelection(redAlarm.getAlarmTone());

            repeatWeeklyButton.setChecked(redAlarm.isEnableRepeat());
            setWeekDaySelectEnabled(redAlarm.isEnableRepeat());

            alarmMessageEditText.setText(redAlarm.getMessage());
        }else{
            if(getResources().getBoolean(R.bool.debug)== true){
                Log.d("RED ALARM", "Null Red Alarm Object");
            }
        }

    }
    private void syncDataToRedAlarm(){
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Syncing data to red alarm...");
        }

        if(redAlarm !=null){
            redAlarm.clearActiveDays();
            for(int i=0; i<weekdayButtons.length; ++i){
                if(weekdayButtons[i].isChecked()){
                    redAlarm.addActiveDay(indexToDayOfWeek(i));
                }
            }
            redAlarm.setWakeUpTestType(wakeUpActivitySpinner.getSelectedItemPosition());
            redAlarm.setAlarmTone(alarmToneSpinner.getSelectedItemPosition());
            redAlarm.setEnableRepeat(repeatWeeklyButton.isChecked());

            redAlarm.setMessage(alarmMessageEditText.getText().toString());
        }else{
            if(getResources().getBoolean(R.bool.debug)== true){
                Log.d("RED ALARM", "Null Red Alarm Object");
            }
        }

    }
    private int dayOfWeekToIndex(RedAlarm.Day dayOfWeek){
        switch (dayOfWeek){
            case SUN:{
                return 0;
            }
            case MON:{

                return 1;
            }
            case TUE:{

                return 2;
            }
            case WED:{

                return 3;
            }
            case THUR:{

                return 4;
            }
            case FRI:{

                return 5;
            }
            case SAT:{

                return 6;
            }
            default: return -1;
        }
    }

    private RedAlarm.Day indexToDayOfWeek(int index){
        switch (index){
            case 0:{

                return RedAlarm.Day.SUN;
            }
            case 1:{

                return RedAlarm.Day.MON;
            }
            case 2:{

                return RedAlarm.Day.TUE;
            }
            case 3:{

                return RedAlarm.Day.WED;
            }
            case 4:{

                return RedAlarm.Day.THUR;
            }
            case 5:{

                return RedAlarm.Day.FRI;
            }
            case 6:{

                return RedAlarm.Day.SAT;
            }
            default: return null;
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "On timeset called..");
        }
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.set(Calendar.HOUR_OF_DAY,hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        redAlarm.setOffTimeFromMillis(c.getTimeInMillis());
        alarmTimeTextView.setText(redAlarm.getOffTime().format("%H:%M"));
    }
    private void setWeekDaySelectEnabled(boolean enabled){
        for(CheckBox c: weekdayButtons){
            c.setEnabled(enabled);
        }
        for(TextView t: weekdayButtonLabels){
            t.setEnabled(enabled);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch(buttonView.getId()){
            case R.id.repeatCheckBox:{
                setWeekDaySelectEnabled(isChecked);
                break;
            }
            case R.id.sundayButton:{
                setWeekdayLabelEnabled(0, isChecked);
                break;
            }
            case R.id.mondayButton:{
                setWeekdayLabelEnabled(1, isChecked);
                break;
            }
            case R.id.tuesdayButton:{
                setWeekdayLabelEnabled(2, isChecked);
                break;
            }
            case R.id.wednesdayButton:{
                setWeekdayLabelEnabled(3, isChecked);
                break;
            }
            case R.id.thursdayButton:{
                setWeekdayLabelEnabled(4, isChecked);
                break;
            }case R.id.fridayButton:{
                setWeekdayLabelEnabled(5, isChecked);
                break;
            }
            case R.id.saturdayButton:{
                setWeekdayLabelEnabled(6, isChecked);
                break;
            }

        }


    }

    private void setWeekdayLabelEnabled(int weekDay, boolean enabled){
        if(weekDay >=0 && weekDay <7){
            if(enabled){
                weekdayButtonLabels[weekDay].setText(WEEKDAYS_UNDERLINED[weekDay]);
            }else{
                weekdayButtonLabels[weekDay].setText(WEEKDAYS[weekDay]);
            }
        }
    }

    private class WeekdayClickListener implements View.OnClickListener{
        private int dayOfWeek;

        WeekdayClickListener(int dayOfWeek){
            if(dayOfWeek >=0 && dayOfWeek <7)
            this.dayOfWeek = dayOfWeek;
        }
        @Override
        public void onClick(View v) {
            weekdayButtons[dayOfWeek].performClick();
        }
    }
}
