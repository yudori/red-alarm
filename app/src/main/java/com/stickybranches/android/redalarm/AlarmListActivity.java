package com.stickybranches.android.redalarm;

import android.app.AlarmManager;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;
import com.stickybranches.android.redalarm.database.AlarmDbHelper;
import com.stickybranches.android.redalarm.database.AlarmDbManager;


public class AlarmListActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private AlarmListAdapter adapter;
    AlarmDbHelper dh;
    private ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dh = new AlarmDbHelper(this);

        adapter = new AlarmListAdapter(this);
        lv = (ListView)findViewById(R.id.alarm_list_view);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        registerForContextMenu(lv);
        
    }

    @Override
    protected void onResume(){
        super.onResume();
        adapter.reInitialize();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.context_menu_edit:
                startEditActivity(info.id);
                return true;
            case R.id.context_menu_delete:
                RedAlarm alarm = AlarmDbManager.executeReadById(dh, info.id);
                alarm.delete((AlarmManager)getSystemService(ALARM_SERVICE), dh);
                onResume();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_add) {
            Intent intent = new Intent(this, AlarmViewActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("Red Alarm", "On Item Click");
        startEditActivity(id);
    }

    public void startEditActivity(long id){
        Uri uri = Uri.parse(AlarmDbHelper.RED_ALARM_PATH);
        uri = ContentUris.withAppendedId(uri,id);
        Intent intent = new Intent(this, AlarmViewActivity.class);
        intent.setData(uri);
        startActivity(intent);
    }
}
