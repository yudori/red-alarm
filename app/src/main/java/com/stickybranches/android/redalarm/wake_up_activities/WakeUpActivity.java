package com.stickybranches.android.redalarm.wake_up_activities;

import com.stickybranches.android.redalarm.AlarmService;
import com.stickybranches.android.redalarm.basic_entities.RedAlarm;
import com.stickybranches.android.redalarm.wake_up_activities.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.stickybranches.android.redalarm.R;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class WakeUpActivity extends Activity {
    // private methods
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;


    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;
    private Intent alarmPlayerIntent;

    //protected methods
    /**
     * (@Link TextView) for displaying alarm Message.
     */
    private TextView alarmMessageTextView;
    /**
     * boolean variable to determine if subclasses called the setAlarmMessageTextView()
     */
    private boolean hasSetAlarmMessageTextViewBeenCalled = false;


    /**
     * onCreate method provides common settings for all wake up activities
     * should not be overridden
     * Instead, you must override onCreateWakeUpTest()
     *
     * @param savedInstanceState
     */
    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "WakeUp Activity Called");
        }
        onCreateWakeUpTest(savedInstanceState);

        // all subclasses must have a root layout R.id.fullscreen_content
        final View contentView = findViewById(R.id.fullscreen_content);
        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        if(!hasSetAlarmMessageTextViewBeenCalled){
            throw new IllegalStateException("You must call setAlarmMessageTextView(TextView) from onCreateTest");
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD +
        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED+
        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        alarmMessageTextView.setText(getIntent().getStringExtra(RedAlarm.ALARM_MESSAGE));
        playAlarmTone();
    }

    /**
     * Method overridden to disable back press
     */
    @Override
    public final void onBackPressed() {
        //do nothing to ignore back navigation
    }



    /**
     * Sets the textView used for displaying the alarmMessage.
     * All subclasses must have this textView and must call this method to register the textView
     *
     * @param alarmTextView textView to display the alarm message
     */
    public final void setAlarmMessageTextView(TextView alarmTextView){
        if(alarmTextView != null) {
            alarmMessageTextView = alarmTextView;
            hasSetAlarmMessageTextViewBeenCalled = true;
        }
    }

    /**
     * Called during onCreate. Subclasses are not allowed to override onCreate method but this method.
     * A layout with a root view of id R.id.fullscreen_content has to be created
     *
     * @param savedInstanceState
     */
    protected void onCreateWakeUpTest(Bundle savedInstanceState){
        setContentView(R.layout.activity_wake_up);
        TextView textView = (TextView) findViewById(R.id.alarmMessageTextView);
        setAlarmMessageTextView(textView);
        Button button = (Button) findViewById(R.id.stopTestButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopTest();
            }
        });
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "WakeUpTest Activity onCreateTest Called");
        }
    }

    /**
     * plays alarm tone
     * Starts the (@Link AlarmService) that loops the tone continuously
     */
    private void playAlarmTone(){
       alarmPlayerIntent = new Intent(this, AlarmService.class);
        alarmPlayerIntent.putExtra(RedAlarm.APP_TONE,getIntent().getStringExtra(RedAlarm.APP_TONE));
        startService(alarmPlayerIntent);
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM","Alarm Tone Play started");
        }
    }

    /**
     * stops the alarm
     */
    private void stopAlarmTone(){
       stopService(alarmPlayerIntent);
        if(getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM","Alarm Tone Stopped");
        }
    }

    /**
     * used as a signal to stop the test.
     * All sub-classes must call this method once the test is finished.
     * cannot be overridden
     */
    public final void stopTest(){
        stopAlarmTone();
        finish();
    }
}
