package com.stickybranches.android.redalarm;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;
import com.stickybranches.android.redalarm.database.AlarmDbHelper;
import com.stickybranches.android.redalarm.database.AlarmDbManager;

import java.util.Calendar;
import java.util.TreeSet;

/**
 * Created by Gerald on 20/01/2015.
 *
 * AlarmReceiver receives broadcasts from AlarmManger once an alarm is fired.
 * AlarmReceiver checks if the day of the week is amongst the one indicated in the list for repeating alarm.
 *
 */
public class AlarmReceiver extends BroadcastReceiver {
    private AlarmDbHelper helper;
    public static final int ALARM_DELAY_TOLERANCE_MILLIS= 600000;
    @Override
    public void onReceive(Context context, Intent intent) {
        if(context.getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Alarm Receiver onReceive called");
        }
        Uri uri = intent.getData();
        helper = new AlarmDbHelper(context);

        if (uri.getAuthority().equals(AlarmDbHelper.AUTHORITY)) {
            long id = ContentUris.parseId(uri);
            if(id >0 ) {
                if(context.getResources().getBoolean(R.bool.debug)== true){
                    Log.d("RED ALARM", "Correct Alarm Id received");
                }
                RedAlarm redAlarm = AlarmDbManager.executeReadById(helper, id);
                if (redAlarm != null) {
                    TreeSet<RedAlarm.Day> days = redAlarm.getActiveDaysOfWeek();
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(System.currentTimeMillis());
                    if(redAlarm.isEnableRepeat()) {
                        if(context.getResources().getBoolean(R.bool.debug)== true){
                            Log.d("RED ALARM", "Repeating alarm fired");
                        }
                        if (days.contains(RedAlarm.toRedAlarmDay(cal.get(Calendar.DAY_OF_WEEK)))) {
                            if (context.getResources().getBoolean(R.bool.debug) == true) {
                                Log.d("RED ALARM", "Day of Week is that selected");
                            }
                            startAlarm(context, redAlarm);

                        }
                    }else{
                        if(context.getResources().getBoolean(R.bool.debug)== true){
                            Log.d("RED ALARM", "Non-Repeating alarm fired");
                        }
                        startAlarm(context,redAlarm);
                    }
                }
            }
        }

    }

    /**
     * Starts the WakeUpTestActivity indicated in the RedAlarm object once time elapses
     * @param context activity context
     * @param redAlarm RedAlarm object to fire
     */
    private static void startAlarm(Context context, RedAlarm redAlarm) {
        if(context.getResources().getBoolean(R.bool.debug)== true){
            Log.d("RED ALARM", "Start Alarm Called");
        }
        long timeDelay = System.currentTimeMillis() - redAlarm.getOffTimeInMillis();
        if(timeDelay < ALARM_DELAY_TOLERANCE_MILLIS) {
            Class<?> c = null;
            String className = redAlarm.getWakeUpActivityClassName();
            if (context.getResources().getBoolean(R.bool.debug) == true) {
                Log.d("RED ALARM", String.format("Classname = %s", className));
            }
            if (className != null) {
                try {
                    c = Class.forName(className);
                    Intent intent = new Intent(context, c);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );

                    intent.putExtra(RedAlarm.APP_TONE, redAlarm.getAlarmTonePaths());
                    intent.putExtra(RedAlarm.ALARM_MESSAGE, redAlarm.getMessage());
                    context.startActivity(intent);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }else{
            if (context.getResources().getBoolean(R.bool.debug) == true) {
                Log.d("RED ALARM", "Alarm Time Has passed");
            }
        }
    }
}
