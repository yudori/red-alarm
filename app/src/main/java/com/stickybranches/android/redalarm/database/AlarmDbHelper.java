package com.stickybranches.android.redalarm.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Database Helper Class to initialize database
 */
public class AlarmDbHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "Alarm";
    private static final String DATABASE_NAME = "red_alarm_database";
    private static final int DATABASE_VERSION = 2;
    public static final String AUTHORITY = "com.stickybranches.android.redalarm.database";
    public static final String RED_ALARM_PATH = "red_alarm";
    public Context context;

    private static String CREATE_QUERY = "CREATE TABLE "+TABLE_NAME+" (" +
            TableColumns.ID+" INTEGER PRIMARY KEY AUTOINCREMENT," +
            TableColumns.MESSAGE+" VARCHAR(255)," +
            TableColumns.DAYS+" INTEGER," +
            TableColumns.TIME+" INTEGER," +
            TableColumns.ALARM_TONE+" INTEGER," +
            TableColumns.WAKE_TYPE +" INTEGER," +
            TableColumns.REPEAT+" BOOLEAN," +
            TableColumns.ENABLED+" BOOLEAN)";

    private static String DROP_QUERY = "DROP TABLE IF EXISTS "+TABLE_NAME;

    public AlarmDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        Log.d("AlarmDatabase", "Constructor called");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL(CREATE_QUERY);
            Log.d("AlarmDatabase", "On create called");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_QUERY);
            onCreate(db);
            Log.d("AlarmDatabase", "On Upgrade called");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }


    class TableColumns{
        static final String ID = "_id";
        static final String MESSAGE = "message";
        static final String DAYS = "days_of_week";
        static final String TIME = "time";
        static final String ALARM_TONE = "alarm_tone";
        static final String WAKE_TYPE = "wake_up_test_id";
        static final String REPEAT = "repeat";
        static final String ENABLED = "enabled";
    }
}

