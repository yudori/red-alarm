package com.stickybranches.android.redalarm.basic_entities;

import android.view.animation.Interpolator;

import java.util.Random;

/**
 * Created by Gerald on 03/03/2015.
 */
public class RandomInterpolator implements Interpolator {
    int previousValue;
    float previousInterpolation;
    Random rand;
    public RandomInterpolator(){
        rand = new Random(System.currentTimeMillis());
        previousValue = 0;
        previousInterpolation =0;
    }
    public RandomInterpolator(long seed){
        rand = new Random(seed);
        previousValue = 0;
        previousInterpolation =0;
    }
    @Override
    public float getInterpolation(float input) {
        int value = (int) Math.floor(input * 10);
        if(value != previousValue){
            previousInterpolation = rand.nextFloat();

        }
        previousValue = value;
        return previousInterpolation;
    }
}
