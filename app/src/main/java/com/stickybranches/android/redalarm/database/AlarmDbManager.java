package com.stickybranches.android.redalarm.database;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;

import java.util.ArrayList;
import java.util.List;


/**
 *  Class provides static methods to interact with an AlarmDbHelper object and
 *  perform basic operations such as insertion, update, deletion and retrieval
 */
public class AlarmDbManager {

    /**
     * Inserts a row into alarm database
     *
     * @param dh AlarmDbHelper object
     * @param alarm RedAlarm object
     * @return index of row inserted, -1 if an error occurs
     */
    public static long executeInset(AlarmDbHelper dh, RedAlarm alarm){
        SQLiteDatabase database = dh.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(AlarmDbHelper.TableColumns.MESSAGE, alarm.getMessage());
        cv.put(AlarmDbHelper.TableColumns.DAYS, alarm.daysToInteger());
        cv.put(AlarmDbHelper.TableColumns.TIME, alarm.getOffTimeInMillis());
        cv.put(AlarmDbHelper.TableColumns.ALARM_TONE, alarm.getAlarmTone());
        cv.put(AlarmDbHelper.TableColumns.WAKE_TYPE, alarm.getWakeUpTestType());
        cv.put(AlarmDbHelper.TableColumns.REPEAT, alarm.isEnableRepeat());
        cv.put(AlarmDbHelper.TableColumns.ENABLED, alarm.isEnabled());
        long rowId = database.insert(AlarmDbHelper.TABLE_NAME, null, cv);
        if(rowId != -1){
            alarm.setId(rowId);

        }
        return rowId;
    }

    /**
     * Updates a row in alarm database
     *
     * @param dh AlarmDbHelper object
     * @param alarm RedAlarm object
     * @return index of row updated, -1 if an error occurs
     */
    public static long executeUpdate(AlarmDbHelper dh, RedAlarm alarm){
        SQLiteDatabase database = dh.getWritableDatabase();
        ContentValues cv = new ContentValues();
        String[] whereArgs = {""+alarm.getId()};
        cv.put(AlarmDbHelper.TableColumns.MESSAGE, alarm.getMessage());
        cv.put(AlarmDbHelper.TableColumns.DAYS, alarm.daysToInteger());
        cv.put(AlarmDbHelper.TableColumns.TIME, alarm.getOffTimeInMillis());
        cv.put(AlarmDbHelper.TableColumns.ALARM_TONE, alarm.getAlarmTone());
        cv.put(AlarmDbHelper.TableColumns.WAKE_TYPE, alarm.getWakeUpTestType());
        cv.put(AlarmDbHelper.TableColumns.REPEAT, alarm.isEnableRepeat());
        cv.put(AlarmDbHelper.TableColumns.ENABLED, alarm.isEnabled());
        return database.update(AlarmDbHelper.TABLE_NAME, cv, AlarmDbHelper.TableColumns.ID+"=?", whereArgs);

    }

    /**
     * Gets/Reads database rows
     *
     * @param dh AlarmDbHelper object
     * @return Cursor object for alarm table
     */
    public static Cursor executeRead(AlarmDbHelper dh){
        SQLiteDatabase database = dh.getReadableDatabase();
        String[] columns = {AlarmDbHelper.TableColumns.ID, AlarmDbHelper.TableColumns.MESSAGE, AlarmDbHelper.TableColumns.DAYS, AlarmDbHelper.TableColumns.TIME,
                AlarmDbHelper.TableColumns.ALARM_TONE, AlarmDbHelper.TableColumns.WAKE_TYPE, AlarmDbHelper.TableColumns.REPEAT, AlarmDbHelper.TableColumns.ENABLED};
        return database.query(AlarmDbHelper.TABLE_NAME, columns, null, null, null, null, null);
    }
    /**
     * Gets/Reads database rows
     *
     * @param dh AlarmDbHelper object
     * @param alarmID RedAlarm id of alarm row to be read
     * @return RedAlarm object for alarm table row
     */
    public static RedAlarm executeReadById(AlarmDbHelper dh, long alarmID){
        SQLiteDatabase database = dh.getReadableDatabase();
        String[] columns = {AlarmDbHelper.TableColumns.ID, AlarmDbHelper.TableColumns.MESSAGE, AlarmDbHelper.TableColumns.DAYS, AlarmDbHelper.TableColumns.TIME,
                AlarmDbHelper.TableColumns.ALARM_TONE, AlarmDbHelper.TableColumns.WAKE_TYPE, AlarmDbHelper.TableColumns.REPEAT, AlarmDbHelper.TableColumns.ENABLED};
        Cursor cursor = database.query(AlarmDbHelper.TABLE_NAME, columns, AlarmDbHelper.TableColumns.ID+"="+alarmID, null, null, null, null);
        RedAlarm alarm = new RedAlarm(dh.context);
        if (cursor.getCount() == 0)
            return alarm;
        cursor.moveToFirst();
        alarm.setId(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.ID)));
        alarm.setMessage(cursor.getString(cursor.getColumnIndex(AlarmDbHelper.TableColumns.MESSAGE)));
        alarm.setActiveDaysFromInteger(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.DAYS)));
        alarm.setOffTimeFromMillis(cursor.getLong(cursor.getColumnIndex(AlarmDbHelper.TableColumns.TIME)));
        alarm.setAlarmTone(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.ALARM_TONE)));
        alarm.setWakeUpTestType(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.WAKE_TYPE)));
        alarm.setEnableRepeat(false);
        if (cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.REPEAT)) == 1)
            alarm.setEnableRepeat(true);
        alarm.setEnabled(false);
        if (cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.ENABLED)) == 1)
            alarm.setEnabled(true);
        cursor.close();
        return alarm;
    }

    /**
     * Reads all database alarm entries
     *
     * @param dh AlarmDbHelper instance
     * @return List of alarms in database
     */
    public static List<RedAlarm> executeReadAll(AlarmDbHelper dh){
        List<RedAlarm> alarms = new ArrayList<>();

        SQLiteDatabase database = dh.getReadableDatabase();
        String[] columns = {AlarmDbHelper.TableColumns.ID, AlarmDbHelper.TableColumns.MESSAGE, AlarmDbHelper.TableColumns.DAYS, AlarmDbHelper.TableColumns.TIME,
                AlarmDbHelper.TableColumns.ALARM_TONE, AlarmDbHelper.TableColumns.WAKE_TYPE, AlarmDbHelper.TableColumns.REPEAT, AlarmDbHelper.TableColumns.ENABLED};
        Cursor cursor = database.query(AlarmDbHelper.TABLE_NAME, columns, null, null, null, null, null);

        while(cursor.moveToNext()) {
            RedAlarm alarm = new RedAlarm(dh.context);
            alarm.setId(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.ID)));
            alarm.setMessage(cursor.getString(cursor.getColumnIndex(AlarmDbHelper.TableColumns.MESSAGE)));
            alarm.setActiveDaysFromInteger(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.DAYS)));
            alarm.setOffTimeFromMillis(cursor.getLong(cursor.getColumnIndex(AlarmDbHelper.TableColumns.TIME)));
            alarm.setAlarmTone(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.ALARM_TONE)));
            alarm.setWakeUpTestType(cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.WAKE_TYPE)));
            alarm.setEnableRepeat(false);
            if (cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.REPEAT)) == 1)
                alarm.setEnableRepeat(true);
            alarm.setEnabled(false);
            if (cursor.getInt(cursor.getColumnIndex(AlarmDbHelper.TableColumns.ENABLED)) == 1)
                alarm.setEnabled(true);
            alarms.add(alarm);
        }
        cursor.close();
        return alarms;
    }

    /**
     * Deletes row element for alarm table
     * @param dh AlarmDbHelper object
     * @param alarmID ID of row to be deleted
     * @return number of rows affected
     */
    public static int executeDelete(AlarmDbHelper dh, long alarmID){
        SQLiteDatabase database = dh.getWritableDatabase();
        String[] whereArgs = {""+alarmID};
        return database.delete(AlarmDbHelper.TABLE_NAME, AlarmDbHelper.TableColumns.ID+"=?", whereArgs);

    }

}
