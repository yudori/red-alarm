package com.stickybranches.android.redalarm;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.stickybranches.android.redalarm.basic_entities.RedAlarm;
import com.stickybranches.android.redalarm.database.AlarmDbHelper;
import com.stickybranches.android.redalarm.database.AlarmDbManager;

import java.util.List;

/**
 * Created by Gerald on 27/01/2015.
 */
public class BootReceiver extends BroadcastReceiver {
    AlarmDbHelper dbHelper;
    AlarmManager manager;
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            if (context.getResources().getBoolean(R.bool.debug) == true) {
                Log.d("RED ALARM", "Boot Receiver called");
            }
            dbHelper = new AlarmDbHelper(context);
            manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            List<RedAlarm> redAlarms = AlarmDbManager.executeReadAll(dbHelper);
            if(!redAlarms.isEmpty()) {
                for (RedAlarm alarm : redAlarms) {
                    if (alarm.isEnabled()) {
                        if (!alarm.isEnableRepeat() && (alarm.getOffTimeInMillis() < System.currentTimeMillis())) {
                            continue;
                        }

                        alarm.schedule(manager, dbHelper);
                    }
                }
            }
        }
    }
}
